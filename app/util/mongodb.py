from pymongo import MongoClient
import config

class MongoCon(object):
   __db = None

   @classmethod
   def get_connection(cls):
       global configuration
       if cls.__db is None:
           print(config.DB)
           print(config.USERNAME)
           client = MongoClient(config.MONGOCLIENT,
               username=config.USERNAME,
               password=config.PASSWORD,
               authSource=config.DB)    
           #cls.__db = client['youplus_development']
           cls.__db = client[config.DB]
       return cls.__db