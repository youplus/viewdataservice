import config
from service import ViewDataService
from MessageBroker import MessageBrokerFactory
import json
import datetime
import threading
import copy
import os 
import time
from bson.objectid import ObjectId


class Task():
    host = config.QUEUE_HOST
    userName = config.QUEUE_USER_NAME
    password = config.QUEUE_PASSWORD
    port = config.QUEUE_PORT

    def __init__(self):
        pass


    def publish(self, data, queue, routeKey, exchangeName, exchangeType):
        Broker = self.createBroker(queue, exchangeName, exchangeType)
        Broker.queue_bind(queue, exchangeName, routeKey)
        Broker.publish(exchangeName, routeKey , data)
        Broker.close_connection()
    
    def subscribe(self):
        queue = config.SUBSCRIBE_QUEUE_NAME
        exchangeName = config.SUBSCRIBE_QUEUE_EXCHANGE_NAME
        exchangeType = config.SUBSCRIBE_QUEUE_EXCHANGE_TYPE
        exchangeKey = config.SUBSCRIBE_ROUTE_KEY
        Broker = self.createBroker(queue, exchangeName, exchangeType)
        Broker.queue_bind(queue, exchangeName,exchangeKey)     
        Broker.subscribe(exchangeName, queue, self.callback)

    def createBroker(self, queue, exchange, exchangeType):
        subscribeBroker = MessageBrokerFactory()
        Broker = subscribeBroker.get_broker('rabbitmq')
        connection = Broker.build_broker(self.host, self.port, self.userName, self.password)
        Broker.open_connection(connection)
        Broker.declare_queue(queue)
        Broker.declare_exchange(exchange,exchangeType)
        return Broker

    def callback(self,channel, method, properties, body):
        app = config.APP_NAME
        try:
            data = json.loads(body)
            # this solves the issue of pikka time out when the process takes more than 180 sec
            thread = threading.Thread(target=self.process_data, args=(data,))
            thread.start()
            while thread.is_alive():  # Loop while the thread is processing
                channel._connection.sleep(1.0)
            channel.basic_ack(delivery_tag=method.delivery_tag)      
        except Exception as e:
            print(e)
            data = json.loads(body)
            config.logging.error('{0} exception for ID {1} : {2}'.format(app, data, e))
            errorData = self.process_error(data, str(e))
            self.publish(errorData,config.ERROR_QUEUE_NAME, config.ERROR_ROUTE_KEY, config.ERROR_QUEUE_EXCHANGE_NAME, config.ERROR_QUEUE_EXCHANGE_TYPE)
            

    def process_output(self, output, data):
     
        if output is not None:      
            if output['is_ecom']:
                self.publish(data,config.PUBLISH_ECOM_QUEUE_NAME,config.PUBLISH_ECOM_ROUTE_KEY,config.PUBLISH_ECOM_QUEUE_EXCHANGE_NAME, config.PUBLISH_ECOM_EXCHANGE_TYPE)
            else:
                print("Non-ecom data stopped publishing to search queue")
                #prepare data for crawle
                #self.publish(data,config.PUBLISH_NON_ECOM_QUEUE_NAME, config.PUBLISH_NON_ECOM_ROUTE_KEY, config.PUBLISH_NON_ECOM_QUEUE_EXCHANGE_NAME, config.PUBLISH_NON_ECOM_EXCHANGE_TYPE)
      
        else:
            error = "error in output"
            config.logging.error('{0} exception for ID {1} : {2}'.format(config.APP_NAME, data, error))
            errorData = self.process_error(data, error)
            self.publish(errorData,config.ERROR_QUEUE_NAME, config.ERROR_ROUTE_KEY, config.ERROR_QUEUE_EXCHANGE_NAME, config.ERROR_QUEUE_EXCHANGE_TYPE)


    def process_data(self, data):
        try:
            import time
            print("process data")
            if '_id' in data:
                output = ViewDataService.insertIntoWidget(data, True)
            else:
                output = ViewDataService.insertIntoWidget(data, False)

                if not output["is_ecom"]:
                    crawlerData={}
                    crawlerData["Provider"]="bing"
                    crawlerData["RecordSet"]=50
                    crawlerData["CountryCode"]="IN, US"
                    crawlerData["Language"]="en"
                    crawlerData["PayloadType"]="search"
                    crawlerData["Taxonomy"]=""
                    crawlerData["PermuteKw"]=False
                    crawlerData["ReportParams"]=""
                    crawlerData["entities"] = data["entities"]
                    crawlerData["key_phrases"] = data["key_phrases"]
                    data = crawlerData

                if ("entities" in data.keys()) and ("key_phrases" in data.keys()):
                    keywords = list(set([x['entity'] for x in data["entities"]] + [x['key_phrase'] for x in data["key_phrases"]]))   # set: remove duplicates
                    data.pop('entities', None)
                    data.pop('key_phrases', None)
                    for key in keywords:
                        data["Keyword"] = key
                        jdata = json.dumps(data)
                        self.process_output(output, jdata)
                        print('published at - ' + str(time.time()))
                else:
                    jdata = json.dumps(data)
                    self.process_output(output, jdata)
                    print('published at - ' + str(time.time()))
        except Exception as e:
            print(e)
            config.logging.error('{0} exception for ID {1} : {2}'.format(config.APP_NAME, data, e))
            errorData = self.process_error(data, str(e))
            self.publish(errorData,config.ERROR_QUEUE_NAME, config.ERROR_ROUTE_KEY, config.ERROR_QUEUE_EXCHANGE_NAME, config.ERROR_QUEUE_EXCHANGE_TYPE)

    
    def process_error(self, data, error):
        currentDT = datetime.datetime.now()
        data['errorTime'] = currentDT.strftime("%Y-%m-%d %H:%M:%S")
        data['api'] = config.APP_NAME
        data['error'] = error
        return json.dumps(data)


if __name__ == '__main__':
    task = Task()
    print("running task")
    task.subscribe()