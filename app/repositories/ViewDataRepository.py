from util import mongodb
import config
import copy
from bson.objectid import ObjectId
from datetime import datetime
mdb = mongodb.MongoCon()
db = mdb.get_connection()
widget_config_col = db[config.WIDGET_CONFIG_COLLECTION]
categories_col = db[config.CATEGORIES_COLLECTION]
print("Initialised db")


class ViewDataRepository():


    
    def __init__(self):
        pass

    def insertIntoWidget(self, data, widget_flag):
        print("insert Into Widget Repo")
        if widget_flag:
            widget_id = data.pop('_id', None)
            st = widget_config_col.update({'_id': ObjectId(widget_id)}, {"$set": data}, upsert=True)
            return st
        else:
            if 'category_id' in list(data.keys()):
                categoryId = self.read_from_category(data)
                data['category_id'] = categoryId
            if "site_id" in data:
                data["site_id"]= ObjectId(data["site_id"])
            if "user_id" in data:
                data["user_id"]= ObjectId(data["user_id"])

            #data['view_status'] = "in_q" if data['is_ecom'] else "preparing"    # For ecom => 'view_status':'in_q' and for Non-ecom => 'view_status':'preparing'
            if data["is_ecom"] == True:
                data['view_status'] = "preparing"    # For both Ecom and Non ecom, status is going to set for "preparing" 
            else:
                data['view_status'] = "live"
            data["created_at"] = datetime.now()
            data["video_orders"] = []
            data["uploaded_videos"] = []

            widget_id = data.pop('widget_id', None)
            st = widget_config_col.update({'_id': ObjectId(widget_id)}, {"$set": data}, upsert=True)

            data["widget_id"]=str(widget_id)
            data["site_id"]=str(data["site_id"])
            if "user_id" in data: data["user_id"]=str(data["user_id"])

            data.pop('view_status', None)
            data.pop('created_at', None)
            data.pop('video_orders', None)
            data.pop('uploaded_videos', None)
            return data

    def read_from_category(self, data):
        category_data = list(categories_col.find({"model_id": data["category_id"]}))
        if len(category_data)!=0:
            categoryId = category_data[0]['_id']
        else:
            categoryId = ObjectId("000000000000000000000000")
        return categoryId

